opencv-python~=4.9.0.80
numpy~=1.26.4
pandas~=2.2.1
cvzone~=1.6.1
ultralytics~=8.1.24
pillow~=10.2.0
tkmacosx~=1.0.5