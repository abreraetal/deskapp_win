import cv2
import firebase_admin
from firebase_admin import credentials, db
import base64
import time

# Firebase credentials and initialization
cred = credentials.Certificate("patpat-a2258-firebase-adminsdk-sfbtb-1705d584a0.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://patpat-a2258-default-rtdb.asia-southeast1.firebasedatabase.app/'
})

# Create a VideoCapture object
cap = cv2.VideoCapture(0)  # Use 0 for default webcam, or pass the path to a video file

# Check if the camera/video stream is opened successfully
if not cap.isOpened():
    print("Error: Could not open video stream")
    exit()

# Reference to Firebase RTDB path
ref = db.reference("/Video/Stream")

# Function to continuously read and send frames
def send_frames():
    while True:
        # Record the start time
        start_time = time.time()

        # Read a frame from the video stream
        ret, frame = cap.read()

        # Check if the frame is successfully read
        if not ret:
            print("Error: Could not read frame")
            break

        # Convert the frame to base64 encoded string
        _, buffer = cv2.imencode('.jpg', frame)
        jpg_as_text = base64.b64encode(buffer)

        # Upload the frame to Firebase RTDB
        ref.set(jpg_as_text.decode('utf-8'))

        # Print a message indicating frame upload
        print("Frame uploaded to Firebase RTDB")

        # Calculate the time taken to process the frame
        elapsed_time = time.time() - start_time

        # Calculate the remaining delay to achieve the desired interval (5 seconds)
        remaining_delay = max(0, 0.01 - elapsed_time)

        # Introduce the remaining delay to achieve the desired interval
        time.sleep(remaining_delay)


# Start the frame uploading process
send_frames()