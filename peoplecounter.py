import cv2
import numpy as np
import pandas as pd
from ultralytics import YOLO
from tracker import Tracker
import cvzone

# Initialize YOLO model
model = YOLO('best.pt')

# Open class list file
with open("coco.txt", "r") as my_file:
    data = my_file.read()
class_list = data.split("\n")

# Define the area for counting
area1 = [(297, 449), (297, 496), (600, 496), (600, 449)]

# Initialize tracker
tracker = Tracker()

counter = []

def detect_people(frame):

    # Resize frame to 1024x576
    frame = cv2.resize(frame, (1024, 576))

    # Predict objects using YOLO model
    results = model.predict(frame)
    a = results[0].boxes.data
    px = pd.DataFrame(a).astype("float")

    # Extract passenger bounding boxes
    passenger_boxes = []
    for index, row in px.iterrows():
        x1 = int(row[0])
        y1 = int(row[1])
        x2 = int(row[2])
        y2 = int(row[3])
        d = int(row[5])
        c = class_list[d]
        if 'Passenger' in c:
            passenger_boxes.append([x1, y1, x2, y2])

    # Update tracker with passenger boxes
    bbox_idx = tracker.update(passenger_boxes)

    # Draw bounding boxes and count passengers
    for id, rect in bbox_idx.items():
        x3, y3, x4, y4 = rect
        cx = x3
        cy = y4
        result = cv2.pointPolygonTest(np.array(area1, np.int32), ((cx, cy)), False)
        if result >= 0:
            cv2.circle(frame, (cx, cy), 6, (0, 255, 0), -1)
            cv2.rectangle(frame, (x3, y3), (x4, y4), (255, 0, 255), 2)
            cvzone.putTextRect(frame, f'{id}', (x3, y3), 1, 1)
            if counter.count(id) == 0:
                counter.append(id)
    cv2.polylines(frame, [np.array(area1, np.int32)], True, (255, 255, 255), 2)
    p = len(counter)
    cvzone.putTextRect(frame, f'Counter:-{p}', (40, 60), 2, 2, (255, 255, 255), (100, 50, 0))

    return frame
