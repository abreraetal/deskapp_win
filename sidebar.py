import tkinter as tk
from tkinter import Button
from PIL import ImageTk, Image

class Sidebar(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.configure(bg="#5b97ca", width=500)

        # Example sidebar content
        self.label = tk.Label(self, text=" Real Time Bus Passenger Counting ", bg="#5b97ca", fg="white", font=("Palatino", 15))
        self.label.pack(pady=10)

        # Get the dimensions of the label
        label_width = self.label.winfo_reqwidth()

        # Create a canvas to draw the line
        self.line_canvas = tk.Canvas(self, bg="lightslategrey", highlightthickness=0, width=label_width, height=2)
        self.line_canvas.pack(fill="x", padx=10)

        # Draw a line at the top of the label
        self.line_canvas.create_line(0, 2, label_width, 2, fill="white", width=2)

        # Load icons
        live_view_icon = Image.open("eye.png")
        playback_icon = Image.open("record.png")
        rtsp_icon = Image.open("wifi.png")

        # Resize icons as needed
        icon_size = (20, 20)
        live_view_icon = live_view_icon.resize(icon_size)
        playback_icon = playback_icon.resize(icon_size)
        rtsp_icon = rtsp_icon.resize(icon_size)

        # Convert icons to PhotoImage objects
        self.live_view_icon_default = ImageTk.PhotoImage(live_view_icon)
        self.playback_icon_default = ImageTk.PhotoImage(playback_icon)
        self.rtsp_icon_default = ImageTk.PhotoImage(rtsp_icon)

        # Load white icons
        live_view_icon_white = Image.open("eye_white.png")
        playback_icon_white = Image.open("record_white.png")
        rtsp_icon_white = Image.open("wifi_white.png")

        # Resize white icons
        live_view_icon_white = live_view_icon_white.resize(icon_size)
        playback_icon_white = playback_icon_white.resize(icon_size)
        rtsp_icon_white = rtsp_icon_white.resize(icon_size)

        # Convert white icons to PhotoImage objects
        self.live_view_icon_white = ImageTk.PhotoImage(live_view_icon_white)
        self.playback_icon_white = ImageTk.PhotoImage(playback_icon_white)
        self.rtsp_icon_white = ImageTk.PhotoImage(rtsp_icon_white)

        # Create buttons for each page
        self.live_view_button = Button(self, text=" Live View                    ", image=self.live_view_icon_default, compound="left", command=lambda: self.on_button_clicked("Live View"), bg="lightslategrey", fg="#193750", font=("Segoe UI", 16), borderwidth=0, anchor="w")
        self.live_view_button.pack(fill="x", padx=(0, 0), pady=(10, 0))

        self.playback_button = Button(self, text=" Playback                    ", image=self.playback_icon_default, compound="left", command=lambda: self.on_button_clicked("Playback"), bg="lightslategrey", fg="#193750", font=("Segoe UI", 16), borderwidth=0, anchor="w")
        self.playback_button.pack(fill="x", padx=(0, 0))

        self.rtsp_button = Button(self, text=" RTSP                          ", image=self.rtsp_icon_default, compound="left", command=lambda: self.on_button_clicked("RTSP"), bg="lightslategrey", fg="#193750", font=("Segoe UI", 16), borderwidth=0, anchor="w")
        self.rtsp_button.pack(fill="x", padx=(0, 0), pady=(0, 0))

        # Variable to store selected item
        self.selected_item = None

        # Call update_button_color to initialize button color
        self.update_button_color()

    def on_button_clicked(self, page_name):
        self.selected_item = page_name
        self.master.show_page(page_name)
        self.update_button_color()

    def update_button_color(self):
        # Reset all buttons to default color
        self.live_view_button.configure(bg="#80afd6", fg="#193750", image=self.live_view_icon_default)
        self.playback_button.configure(bg="#80afd6", fg="#193750", image=self.playback_icon_default)
        self.rtsp_button.configure(bg="#80afd6", fg="#193750", image=self.rtsp_icon_default)

        # Highlight the selected button
        if self.selected_item == "Live View":
            self.live_view_button.configure(bg="dodgerblue", fg="white", image=self.live_view_icon_white)
        elif self.selected_item == "Playback":
            self.playback_button.configure(bg="dodgerblue", fg="white", image=self.playback_icon_white)
        elif self.selected_item == "RTSP":
            self.rtsp_button.configure(bg="dodgerblue", fg="white", image=self.rtsp_icon_white)

    def get_selected_item(self):
        return self.selected_item
