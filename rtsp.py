import tkinter as tk
from PIL import Image, ImageTk
import cv2
from peoplecounter import detect_people

class RtspPage(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.configure(bg="#e3edf6")

        self.label = tk.Label(self, text="Real Time Streaming Protocol", bg="#e3edf6", fg="black", font=("Verdana", 15))
        self.label.pack(pady=10)

        # Create a frame to contain the buttons
        button_frame = tk.Frame(self, bg="#e3edf6")
        button_frame.pack(pady=(10, 0))  # Add padding at the top

        # Define button width and height to match Live View buttons
        button_width = 13
        button_height = 1

        # Create RUN and STOP buttons using tkinter.Button
        self.run_button = tk.Button(button_frame, text="Run", command=self.start_rtsp_processing, bg="mediumseagreen", fg="white", font=("Verdana", 12), width=button_width, height=button_height)
        self.run_button.pack(side=tk.LEFT, padx=(0, 10), pady=(0, 20))  # Add pady to add space at the bottom

        self.stop_button = tk.Button(button_frame, text="Stop", command=self.stop_rtsp_processing, bg="red", fg="white", font=("Verdana", 12), width=button_width, height=button_height)
        self.stop_button.pack(side=tk.RIGHT, padx=(10, 0), pady=(0, 20))  # Add pady to add space at the bottom

        # Create a label widget to display the processed frame
        self.processed_frame_label = tk.Label(self)
        self.processed_frame_label.pack()

        # Set initial states
        self.running = False
        self.cap = None

        # Bind the page visibility event
        self.bind("<Visibility>", self.on_page_visibility)

    def start_rtsp_processing(self):
        if not self.running:
            self.running = True
            self.cap = cv2.VideoCapture(0)  # Change this to 0 for default webcam, or provide the correct path to the video file
            self.display_processed_frame()

    def stop_rtsp_processing(self):
        if self.running:
            self.running = False
            if self.cap:
                self.cap.release()

    def display_processed_frame(self):
        if self.running:
            ret, frame = self.cap.read()  # Read frame from webcam

            if not ret:
                # If end of the video is reached, go back to the beginning
                self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
                return

            # Perform object detection using the detect_people function from people counter.py
            frame = detect_people(frame)

            # Resize frame to fit the label
            frame = cv2.resize(frame, (1024, 576))

            # Convert the frame to RGB format
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

            # Convert the frame to ImageTk format
            img = Image.fromarray(frame)
            imgtk = ImageTk.PhotoImage(image=img)

            # Update the label with the new frame
            self.processed_frame_label.config(image=imgtk)

            # Keep a reference to avoid garbage collection
            self.processed_frame_label.imgtk = imgtk

            # Schedule the next update after a short duration
            self.processed_frame_label.after(10, self.display_processed_frame)

    def on_page_visibility(self, event):
        # When page is not visible, stop processing
        if event.widget.winfo_viewable():
            pass  # Do nothing when page is visible
        else:
            self.stop_rtsp_processing()

def rtsp_content(page_container):
    rtsp_page = RtspPage(page_container)
    return rtsp_page
