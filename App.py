import tkinter as tk
from navbar import Navbar
from sidebar import Sidebar
from liveview import LiveViewPage
from playback import PlaybackPage
from rtsp import RtspPage

class MainApplication(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set window title
        self.title("Cuenta")

        # Set window icon (For Windows, use .ico file format for icons)
        self.iconbitmap("cuenta.ico")  # Replace "cuenta.ico" with the path to your icon file

        # Create navbar
        self.navbar = Navbar(self)
        self.navbar.pack(side="top", fill="x")

        # Create sidebar
        self.sidebar = Sidebar(self)
        self.sidebar.pack(side="left", fill="y")

        # Create container for pages
        self.page_container = tk.Frame(self, bg="#e3edf6")
        self.page_container.pack(side="right", fill="both", expand=True)

        # Initialize pages
        self.live_view_page = LiveViewPage(self.page_container)
        self.playback_page = PlaybackPage(self.page_container)
        self.rtsp_page = RtspPage(self.page_container)

        # Show live view page initially
        self.show_page("Live View")

        # Bind sidebar item selection to show_page method
        self.sidebar.bind("<Double-1>", self.on_sidebar_item_selected)

    def on_sidebar_item_selected(self, event):
        selected_item = self.sidebar.get_selected_item()
        if selected_item == "RTSP":
            # If RTSP page is selected, stop processing before showing the page
            self.live_view_page.stop_liveview_processing()
            self.playback_page.stop_playback_processing()
        elif selected_item == "Playback":
            # If Playback page is selected, stop processing before showing the page
            self.live_view_page.stop_liveview_processing()
            self.rtsp_page.stop_rtsp_processing()
        elif selected_item == "Live View":
            # If Live View page is selected, stop processing before showing the page
            self.rtsp_page.stop_rtsp_processing()
            self.playback_page.stop_playback_processing()
        self.show_page(selected_item)

    def show_page(self, page_name):
        # Hide all pages
        for page in [self.live_view_page, self.playback_page, self.rtsp_page]:
            page.pack_forget()

        if page_name == "Live View":
            self.live_view_page.pack(fill="both", expand=True)
        elif page_name == "Playback":
            self.playback_page.pack(fill="both", expand=True)
        elif page_name == "RTSP":
            self.rtsp_page.pack(fill="both", expand=True)

if __name__ == "__main__":
    app = MainApplication()
    app.geometry("800x600")
    app.mainloop()
