import tkinter as tk
from PIL import Image, ImageTk
import base64
import firebase_admin
import numpy as np
from firebase_admin import credentials, db
from threading import Thread
import cv2
from peoplecounter import detect_people

# Firebase credentials and initialization
cred = credentials.Certificate("patpat-a2258-firebase-adminsdk-sfbtb-1705d584a0.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://patpat-a2258-default-rtdb.asia-southeast1.firebasedatabase.app/'
})

# Reference to Firebase RTDB path
ref = db.reference("/Video/Stream")

class LiveViewPage(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.configure(bg="#e3edf6")
        self.label = tk.Label(self, text="Live View", bg="#e3edf6", fg="black", font=("Verdana", 15))
        self.label.pack(pady=10)

        # Create a frame to contain the buttons
        button_frame = tk.Frame(self, bg="#e3edf6")
        button_frame.pack(pady=(10, 0))  # Add padding at the top

        # Define button width and height
        button_width = 13
        button_height = 1

        # Create a Run button using tkinter.Button
        self.run_button = tk.Button(button_frame, text="Run", command=self.start_liveview_processing, bg="mediumseagreen", fg="white", font=("Verdana", 12), width=button_width, height=button_height)
        self.run_button.pack(side=tk.LEFT, padx=(0, 10), pady=(0, 20))  # Add pady to add space at the bottom

        # Create a Stop button using tkinter.Button
        self.stop_button = tk.Button(button_frame, text="Stop", command=self.stop_liveview_processing, bg="red", fg="white", font=("Verdana", 12), width=button_width, height=button_height)
        self.stop_button.pack(side=tk.RIGHT, padx=(10, 0), pady=(0, 20))  # Add pady to add space at the bottom

        # Create a Label widget to display images
        self.image_label = tk.Label(self)
        self.image_label.pack()

        # Flag to control the object detection loop
        self.running = False

    def start_liveview_processing(self):
        self.running = True
        object_detection_thread = Thread(target=self.perform_object_detection)
        object_detection_thread.daemon = True  # Set the thread as daemon so it will be terminated when the main program exits
        object_detection_thread.start()

    def stop_liveview_processing(self):
        self.running = False

    def perform_object_detection(self):
        while self.running:
            # Read frame from Firebase RTDB
            img_data = ref.get()

            if img_data is not None:
                # Decode base64 encoded image data
                jpg_bytes = base64.b64decode(img_data)

                # Convert bytes to numpy array
                nparr = np.frombuffer(jpg_bytes, dtype=np.uint8)

                # Decode numpy array to OpenCV image format
                frame = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

                # Perform object detection using the detect_people function from people counter.py
                frame = detect_people(frame)

                # Resize frame to fit the label
                frame = cv2.resize(frame, (1024, 576))

                # Convert frame to PIL Image
                img = Image.fromarray(frame)

                # Convert PIL Image to Tkinter PhotoImage
                img_tk = ImageTk.PhotoImage(img)

                # Update the Label widget with the new image
                self.update_image(img_tk)

    def update_image(self, img_tk):
        # If there's already an image displayed, remove it first
        if hasattr(self, "image_label"):
            self.image_label.config(image=None)
            self.image_label.image = None

        # Update the Label widget to display the new image
        self.image_label.config(image=img_tk)
        self.image_label.image = img_tk

def main():
    root = tk.Tk()
    root.title("Live View")
    root.geometry("800x600")

    live_view_page = LiveViewPage(root)
    live_view_page.pack(fill=tk.BOTH, expand=True)

    root.mainloop()

if __name__ == "__main__":
    main()
