import tkinter as tk
from tkinter import filedialog
import cv2
from PIL import Image, ImageTk
from peoplecounter import detect_people

class PlaybackPage(tk.Frame):
    def __init__(self, master, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.config(bg="#e3edf6")

        self.label = tk.Label(self, text="Instant Playback", bg="#e3edf6", fg="black", font=("Verdana", 15))
        self.label.pack(pady=10)

        # Define button width and height to match RTSP buttons
        button_width = 13
        button_height = 1

        # Create a frame to contain the buttons
        button_frame = tk.Frame(self, bg="#e3edf6")
        button_frame.pack(pady=(10, 0))  # Add padding at the top

        # Create an Upload button using tkinter.Button
        self.upload_button = tk.Button(button_frame, text="Upload Video", command=self.upload_video, bg="dodgerblue", fg="white", font=("Verdana", 12), width=button_width, height=button_height)
        self.upload_button.pack(side=tk.LEFT, padx=(0, 10), pady=(0, 20))

        # Create a Stop button using tkinter.Button
        self.stop_button = tk.Button(button_frame, text="Stop", command=self.stop_playback_processing, bg="red", fg="white", font=("Verdana", 12), width=button_width, height=button_height)
        self.stop_button.pack(side=tk.RIGHT, padx=(10, 0), pady=(0, 20))

        self.processed_frame_label = tk.Label(self)
        self.processed_frame_label.pack()

        self.cap = None
        self.paused = False

    def upload_video(self):
        filepath = filedialog.askopenfilename()

        if filepath:
            cap = cv2.VideoCapture(filepath)

            if not cap.isOpened():
                print("Error: Failed to open video file.")
                return

            self.cap = cap
            self.paused = False
            self.display_processed_frame()

    def stop_playback_processing(self):
        if self.cap and self.cap.isOpened():
            self.cap.release()
        self.cap = None
        self.paused = True

    def display_processed_frame(self):
        if self.cap is None or self.paused:
            return

        ret, frame = self.cap.read()

        if not ret:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            return

        frame = detect_people(frame)
        frame = cv2.resize(frame, (1024, 576))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        img = Image.fromarray(frame)
        imgtk = ImageTk.PhotoImage(image=img)

        self.processed_frame_label.config(image=imgtk)
        self.processed_frame_label.imgtk = imgtk

        self.processed_frame_label.after(10, self.display_processed_frame)

def playback_content(page_container):
    playback_page = PlaybackPage(page_container)
    return playback_page
